import { useEffect, useState } from 'react';
import './App.css';

function App() {
  // deciseconds digunakan agar ketika stopwatch di pause interval tidak terulang dari awal
  const [deciseconds, setDeciseconds] = useState(0)
  const [seconds, setSeconds] = useState(0)
  const [minutes, setMinutes] = useState(0)
  const [hours, setHours] = useState(0)
  const [isStopwatchOn, setIsStopwatchOn] = useState(false)

  useEffect(() => {
    let time;
    time = setInterval(() => {
      if(isStopwatchOn) setDeciseconds(deciseconds + 1)
    }, 100);

    if(deciseconds === 10) {
      setDeciseconds(0)
      setSeconds(seconds + 1)
    }

    if(seconds === 60){
      setMinutes(minutes + 1)
      setSeconds(0)
    }

    if(minutes === 60){
      setHours(hours + 1)
      setMinutes(0)
      setSeconds(0)
    }

    return () => clearInterval(time)
  }, [deciseconds, seconds, minutes, hours, isStopwatchOn])

  const onReset = () => {
    setIsStopwatchOn(false)
    setDeciseconds(0)
    setSeconds(0)
    setMinutes(0)
    setHours(0)
  }

  return (
    <section>
      <div className='container'>
        <div className='stopwatch'>
          <p>{hours} : {minutes} : {seconds}</p>
          <p>Jam : Menit : Detik</p>
        </div>
        <div className='buttons'>
          <button className='button-reset' onClick={onReset}>Reset</button>
          <button className='button-start' onClick={() => setIsStopwatchOn(true)}>Start</button>
          <button className='button-stop' onClick={() => setIsStopwatchOn(false)}>Stop</button>
        </div>
      </div>
    </section>
  );
}

export default App;
